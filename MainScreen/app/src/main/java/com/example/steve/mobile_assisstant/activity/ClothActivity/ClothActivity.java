package com.example.steve.mobile_assisstant.activity.ClothActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.LinearLayout;
import com.bumptech.glide.Glide;
import com.example.steve.mobile_assisstant.R;
import com.example.steve.mobile_assisstant.activity.ChoosePhotoActivity.ChoosePhotoActivity;
import com.example.steve.mobile_assisstant.model.ImageProcessorStrategy;
import com.example.steve.mobile_assisstant.model.ImageViewProvider;
import com.example.steve.mobile_assisstant.model.LocalImageProcessor;
import com.example.steve.mobile_assisstant.model.MapWriter;
import com.example.steve.mobile_assisstant.model.ServerModel;
import com.example.steve.mobile_assisstant.model.Suggestion;

import android.view.View.OnClickListener;
import android.view.View;

import com.example.steve.mobile_assisstant.model.URLImageProcessor;
import com.google.gson.Gson;
import com.google.gson.stream.*;
import com.google.gson.reflect.*;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import android.os.Environment;
import retrofit2.Converter;

public class ClothActivity extends AppCompatActivity {
    ServerModel serverModel;
    String clothDescription;
    List<Suggestion> clothSuggestion;
    private String imageMappingFileName = "ImageMapping.txt";
    private String subDir = "UrHelper/ImageDictionary";
    private MapWriter mapWriter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloth);
        Gson gson = new Gson();
        String clothModelStr = getIntent().getStringExtra("ServerModel");

        mapWriter = new MapWriter(imageMappingFileName, subDir);
        //cleanData();
        serverModel = gson.fromJson(clothModelStr, ServerModel.class);
        clothDescription = serverModel.getClothDescription();
        clothSuggestion = serverModel.getClothSuggestion();
        TextView clothoverall = findViewById(R.id.ClothOverallTxt);
        clothoverall.setText(clothDescription);
        LinearLayout main= findViewById(R.id.SuggestionList);
        for (int i = 0 ; i < clothSuggestion.size(); i+=2) {
            LinearLayout row = new LinearLayout(this);
            //row.setLayoutParams( new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            row.setOrientation(LinearLayout.HORIZONTAL);
            for(int j = 0; j <= 1; j++){
                int index = i + j;
                if(index < clothSuggestion.size()){
                    ImageViewProvider ivp = new ImageViewProvider();
                    LinearLayout layout = new LinearLayout(this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    ImageView im = new ImageView(this);
                    im.setId(index);
                    TextView txt = new TextView(this);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(600,600);
                    params.setMargins(10,0,10,0);
                    im.setLayoutParams(params);
                    txt.setText(clothSuggestion.get(index).getName());
                    if(!mapWriter.Exist(clothSuggestion.get(index).getName())) {
                        Glide.with(getApplicationContext()).load(clothSuggestion.get(index).getUrl()).into(im);
                    }
                    else {
                        Glide.with(getApplicationContext()).load(Uri.parse(mapWriter.Get(clothSuggestion.get(index).getName()))).into(im);
                    }
                    layout.addView(im);
                    layout.addView(txt);
                    row.addView(layout);
                    im.setClickable(true);
                    im.setOnClickListener(new ClothImageClickListener(index, this));
                }
            }
            main.addView(row);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode== Activity.RESULT_OK){
            Uri uri = Uri.parse(data.getStringExtra("Uri"));
            ImageView im = findViewById(requestCode);
            Glide.with(getApplicationContext()).load(uri).into(im);
            mapWriter.Set(clothSuggestion.get(requestCode).getName(), data.getStringExtra("Uri"));
        }
    }
    public static class ClothImageClickListener implements OnClickListener{
        private int _id ;
        private Activity _activity ;
        public ClothImageClickListener(int id, Activity activity){
            _activity = activity;
            _id = id;
        }
        @Override
        public void onClick(View view){
            Intent in = new Intent(_activity.getApplicationContext(), ChoosePhotoActivity.class);
            _activity.startActivityForResult(in,_id);
        }
    }
}
