package com.example.steve.mobile_assisstant.model;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Alex on 2018/2/24.
 */

public interface LeoService {
    @GET("Weather")
    Call<ServerModel> getServerModel(@Query("Longitude") Double lon, @Query("Latitude") Double lat);
}
