﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace WebApplication1.Model
{
    public class WeatherBase
    {
  
        public WeatherModel WeatherModel { get; set; }
        public ClothModel ClothModel { get; set; }
        public FoodModel FoodModel { get; set; }
        public ActivityModel ActivityModel { get; set; }

    }
}