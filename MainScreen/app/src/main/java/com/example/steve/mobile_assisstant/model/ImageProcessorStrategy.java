package com.example.steve.mobile_assisstant.model;

import android.graphics.Bitmap;

/**
 * Created by z272l on 3/25/2018.
 */

public interface ImageProcessorStrategy {
    public Bitmap GetDesiredImage();
    public String GetDescription();
}
