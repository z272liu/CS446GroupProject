﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace WebApplication1.Model
{
    public class ConditionWeatherService : IWeatherService<WeatherServiceModel>
    {
        const string UrlBase = "http://api.wunderground.com/api/bed079a6fb121f9c/conditions/q/";
        public WeatherServiceModel GetCityData(string City, string Longitude, string Latitude)
        {
            string parameters = $"{Latitude},{Longitude}.json";
            using (HttpClient client = new HttpClient())
            {
                var httpRequest = new HttpRequestMessage(HttpMethod.Get, UrlBase + parameters);
                var response = client.SendAsync(httpRequest).Result;
                response.EnsureSuccessStatusCode();
                var result = response.Content.ReadAsStringAsync().Result;
                dynamic jsonObj = JsonConvert.DeserializeObject(result);
                return new WeatherServiceModel()
                {
                    Location = jsonObj.current_observation.display_location.city,
                    CityFull = jsonObj.current_observation.display_location.full,
                    WindSpeed = jsonObj.current_observation.wind_string,
                    Temperature = Convert.ToString(jsonObj.current_observation.temp_c),
                    Condition = jsonObj.current_observation.weather,
                    FeelsLike = jsonObj.current_observation.feelslike_c,
                    Humidity = jsonObj.current_observation.relative_humidity,
                };
            }
        }
    }
}