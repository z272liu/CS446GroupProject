package com.example.steve.mobile_assisstant.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Alex on 2018/3/21.
 */

public class ToDoEvent {
    @SerializedName("Date")
    @Expose
    private Date date;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Alarm")
    @Expose
    private Date alarm;

    public ToDoEvent(Date date, String name, String description, Date alarm) {
        this.date = date;
        this.name = name;
        this.description = description;
        this.alarm = alarm;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Date getAlarm() {
        return alarm;
    }
    public void setAlarm(Date alarm) {
        this.alarm = alarm;
    }
}
