package com.example.steve.mobile_assisstant.model;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Suggestion {
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("URL")
    @Expose
    private String url;

    public String getName() {
        return name;
    }
    public String getUrl() {
        return url;
    }
}
