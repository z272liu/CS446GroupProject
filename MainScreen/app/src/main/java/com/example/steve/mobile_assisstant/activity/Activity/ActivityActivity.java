package com.example.steve.mobile_assisstant.activity.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.steve.mobile_assisstant.R;
import com.example.steve.mobile_assisstant.model.ServerModel;
import com.example.steve.mobile_assisstant.model.Suggestion;
import com.google.gson.Gson;

import java.util.List;

public class ActivityActivity extends AppCompatActivity {
    ServerModel serverModel;
    String activityDescription;
    List<Suggestion> activitySuggestion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloth);
        Gson gson = new Gson();
        String foodModelStr = getIntent().getStringExtra("ServerModel");

        //cleanData();
        serverModel = gson.fromJson(foodModelStr, ServerModel.class);
        activityDescription = serverModel.getActivityDescription();
        activitySuggestion = serverModel.getActivitySuggestion();
        TextView activityOverall = findViewById(R.id.ClothOverallTxt);
        activityOverall.setText(activityDescription);
        LinearLayout main= findViewById(R.id.SuggestionList);
        for (int i = 0 ; i < activitySuggestion.size(); i+=2) {
            LinearLayout row = new LinearLayout(this);
            //row.setLayoutParams( new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            row.setOrientation(LinearLayout.HORIZONTAL);
            for(int j = 0; j <= 1; j++){
                int index = i + j;
                if(index < activitySuggestion.size()){
                    LinearLayout layout = new LinearLayout(this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    ImageView im = new ImageView(this);
                    TextView txt = new TextView(this);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(600,600);
                    params.setMargins(10,0,10,0);
                    im.setLayoutParams(params);
                    txt.setText(activitySuggestion.get(index).getName());
                    Glide.with(getApplicationContext()).load(activitySuggestion.get(index).getUrl()).into(im);
                    layout.addView(im);
                    layout.addView(txt);
                    row.addView(layout);
                }
            }
            main.addView(row);
        }
    }

}
