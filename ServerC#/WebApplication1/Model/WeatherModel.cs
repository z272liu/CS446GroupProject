﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace WebApplication1.Model
{
    public class WeatherModel
    {
        public string Location { get; set; }
        public string Temperature { get; set; }
        public string MaxTemperature { get; set; }
        public string MinTemperature { get; set; }
        public string Sunrise { get; set; }
        public string Sunset { get; set; }
        public string WindSpeed { get; set; }
        public string FeelsLike { get; set; }
        public string Humidity { get; set; }
        public string Condition { get; set; }
        public WeatherHourly[] WeatherHourly { get; set; }
        public ForecastModel[] Forecasts { get; set; }
    }
}