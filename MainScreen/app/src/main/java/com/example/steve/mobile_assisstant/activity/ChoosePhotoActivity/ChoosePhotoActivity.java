package com.example.steve.mobile_assisstant.activity.ChoosePhotoActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.steve.mobile_assisstant.R;
import com.example.steve.mobile_assisstant.model.GetImagePath;

import java.io.File;
import java.io.IOException;

/**
 * Created by Alex on 2018/3/24.
 */

public class ChoosePhotoActivity extends AppCompatActivity {
    private final int CAMERA_CODE = 101;
    private final int GALLERY_CODE = 102;
    private final int CROP_CODE = 103;
    private Uri uri = null;
    private File currentImageFile = null;
    private String subDir = "UrHelper/Photo";

    @Override
    protected void onCreate(Bundle savedInstanceState) {  // require dynamic permission application later
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_photo);
    }

    public void chooseFromCamera(View view) {
        File dir = new File(Environment.getExternalStorageDirectory(), subDir);
        //File dir = new File(activity.getFilesDir(), subDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        currentImageFile = new File(dir,System.currentTimeMillis() + ".jpg");
        if(!currentImageFile.exists()){
            try {
                currentImageFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        uri = FileProvider.getUriForFile(this, getFileProvider(), currentImageFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivityForResult(intent, CAMERA_CODE);
    }

    public void chooseFromGallery(View view) {
        File dir = new File(Environment.getExternalStorageDirectory(), subDir);
        //File dir = new File(activity.getFilesDir(), subDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        currentImageFile = new File(dir,System.currentTimeMillis() + ".jpg");
        if(!currentImageFile.exists()){
            try {
                currentImageFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("image/*");
        startActivityForResult(intent, GALLERY_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Intent i = new Intent();
        switch (requestCode) {
            case CAMERA_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    cropPhoto(uri);
                }
                else {
                    currentImageFile.delete();
                    setResult(Activity.RESULT_CANCELED, i);
                }
                break;
            case GALLERY_CODE:
                if (data != null) {
                    File imgUri = new File(GetImagePath.getPath(this, data.getData()));
                    Uri chosenUri = FileProvider.getUriForFile(this, getFileProvider(), imgUri);
                    cropPhoto(chosenUri);
                }
                else {
                    currentImageFile.delete();
                    setResult(Activity.RESULT_CANCELED, i);
                }
                break;
            case CROP_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    i.putExtra("Uri", uri.toString());
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
                else {
                    currentImageFile.delete();
                    setResult(Activity.RESULT_CANCELED, i);
                }
            default:
                break;
        }
    }

    private String getFileProvider() {
        return getPackageName() + ".provider";
    }

    private void cropPhoto(Uri chosen_Uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");

        uri = Uri.fromFile(currentImageFile);
        intent.setDataAndType(chosen_Uri, "image/*");
        intent.putExtra("crop", true);
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 300);
        intent.putExtra("outputY", 300);
        intent.putExtra("return-data", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        //intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivityForResult(intent, CROP_CODE);
    }
}
