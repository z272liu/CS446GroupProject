﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Model
{
    public class ClothDBResult
    {
        //public int id { get; set; }
        public string Description { get; set; }
        public string Condition { get; set; }
        [Key]
        public string Name { get; set; }
        public string URL { get; set; }
    }
}
