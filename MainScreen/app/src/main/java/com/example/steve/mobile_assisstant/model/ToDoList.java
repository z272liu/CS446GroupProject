package com.example.steve.mobile_assisstant.model;

import android.os.Environment;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Alex on 2018/3/21.
 */

public class ToDoList {
    private List<ToDoEvent> toDoEventList;
    private String subDir = "UrHelper/ToDoList";
    File saveFile = null;

    public final ToDoEvent getEvent(int index) {
        return toDoEventList.get(index);
    }
    public Integer getSize (){
        return toDoEventList.size();
    }
    public void add(ToDoEvent toDoEvent) {
        toDoEventList.add(toDoEvent);
        sort();
        writeToLocal();
    }
    public void remove(int index) {
        toDoEventList.remove(index);
        writeToLocal();
    }
    public void set(int index, ToDoEvent toDoEvent) {
        toDoEventList.set(index, toDoEvent);
        sort();
        writeToLocal();
    }
    public ToDoList() {
        File dir = new File(Environment.getExternalStorageDirectory(), subDir);
        //File dir = new File(activity.getFilesDir(), subDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        saveFile = new File(dir, "ToDoListData.txt");
        if (!saveFile.exists()) {
            try {
                saveFile.createNewFile();
                toDoEventList = new ArrayList<ToDoEvent>();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            readFromLocal();
        }
    }
    public void cleanData() {
        saveFile.delete();
        try {
            saveFile.createNewFile();
            toDoEventList.clear();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void sort() {
        Collections.sort(toDoEventList, new Comparator<ToDoEvent>() {
            @Override
            public int compare(ToDoEvent t1, ToDoEvent t2) {
                Date d1 = t1.getDate();
                Date d2 = t2.getDate();
                if (d1.before(d2)) {
                    return -1;
                }
                return 1;
            }
        });
    }
    private void writeToLocal() {  // require dynamic permission application later
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        JsonWriter jw = null;
        try {
            fos = new FileOutputStream(saveFile);
            osw = new OutputStreamWriter(fos, "UTF-8");
            jw = new JsonWriter(osw);
            new Gson().toJson(toDoEventList, new TypeToken<List<ToDoEvent>>(){}.getType(), jw);
            jw.close();
            osw.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (jw != null) {
                    jw.close();
                }
                if (osw != null) {
                    osw.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void readFromLocal() {  // require dynamic permission application later
        FileInputStream fis = null;
        InputStreamReader isr = null;
        JsonReader jr = null;
        try {
            fis = new FileInputStream(saveFile);
            isr = new InputStreamReader(fis);
            jr = new JsonReader(isr);
            toDoEventList = new Gson().fromJson(jr, new TypeToken<List<ToDoEvent>>(){}.getType());
            if (toDoEventList == null) {
                toDoEventList = new ArrayList<ToDoEvent>();
            }
            jr.close();
            isr.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (jr != null) {
                    jr.close();
                }
                if (isr != null) {
                    isr.close();
                }
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
