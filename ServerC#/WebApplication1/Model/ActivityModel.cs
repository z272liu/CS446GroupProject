﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Model
{
    public class ActivityModel
    {
        public string ActivityDescription { get; set; }
        public List<Suggestion> ActivitySuggestion { get; set; }
    }
}
