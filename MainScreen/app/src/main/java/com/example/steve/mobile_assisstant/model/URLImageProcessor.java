package com.example.steve.mobile_assisstant.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by z272l on 3/25/2018.
 */

public class URLImageProcessor implements ImageProcessorStrategy {
    private String _url;
    private final int borderSize = 5;
    private final int width = 170;
    private final int height = 170;
    public URLImageProcessor(String url){
        _url = url;
    }
    @Override
    public Bitmap GetDesiredImage(){
        try {
            Bitmap myBitmap = new DownloadFileTask().execute(_url).get();
            Bitmap bmpWithBorder = Bitmap.createBitmap(myBitmap.getWidth() + borderSize * 2, myBitmap.getHeight() + borderSize * 2, myBitmap.getConfig());
            Canvas canvas = new Canvas(bmpWithBorder);
            canvas.drawColor(Color.RED);
            canvas.drawBitmap(myBitmap, borderSize, borderSize, null);
            return bmpWithBorder;
        } catch (Exception e) {
            // Log exception
            return null;
        }
    }
    @Override
    public String GetDescription(){
        return "hello";
    }
    class DownloadFileTask extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... Url) {
            try {
                URL url = new URL(Url[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(input),width,height,false);
                return myBitmap;
            } catch (Exception e) {

                return null;
            }
        }
    }
}
