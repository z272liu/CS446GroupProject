package com.example.steve.mobile_assisstant.activity.DetailedWeatherActivity;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.steve.mobile_assisstant.R;
import com.example.steve.mobile_assisstant.activity.MainScreenActivity.ToDoListRecyclerViewAdaptor;
import com.example.steve.mobile_assisstant.model.ClothModel;
import com.example.steve.mobile_assisstant.model.HourWeather;
import com.example.steve.mobile_assisstant.model.ServerModel;
import com.example.steve.mobile_assisstant.model.SimpleWeatherModel;
import com.example.steve.mobile_assisstant.model.WeatherModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import pl.droidsonroids.gif.GifTextView;

public class DetailedWeather extends AppCompatActivity {
    ServerModel weatherModel;

    private TextView detailedWeather;
    private TextView detailedFeelsLike;
    private TextView detailedCondition;
    private TextView detailedWind;
    private TextView detailedHumidity;
    private List<HourWeather> listOfHourly = new ArrayList<>();
    private List<SimpleWeatherModel> forecasts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_weather);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Gson gson = new Gson();
        String weatherModelStr = getIntent().getStringExtra("ServerModel");
        weatherModel = gson.fromJson(weatherModelStr, ServerModel.class);

        detailedWeather = (TextView)findViewById(R.id.detailedTemperature);
        detailedWeather.setText(weatherModel.getTemperature()+"°C");
        detailedFeelsLike = (TextView)findViewById(R.id.detailedFeelsLike);
        detailedFeelsLike.setText("Feels Like: "+weatherModel.getFeelsLike()+ "°C");
        detailedCondition = (TextView)findViewById(R.id.detailedCondition);
        detailedCondition.setText(weatherModel.getCondition());
        detailedWind = (TextView)findViewById(R.id.textView2);
        detailedWind.setText("WindSpeed: " + weatherModel.getWindSpeed());
        detailedHumidity = (TextView)findViewById(R.id.textView3);
        detailedHumidity.setText("Humidity: "+weatherModel.getHumidity());

        NestedScrollView nestedScrollView = findViewById(R.id.detailedBackground);
        GifTextView gifTextView =findViewById(R.id.detailedSQ);

        Calendar cal = Calendar.getInstance();
        Integer hour    = cal.get(Calendar.HOUR_OF_DAY);
        Integer minute  = cal.get(Calendar.MINUTE);
        String sunrise = weatherModel.getSunrise();
        String sunset  = weatherModel.getSunset();

        String[] parts = sunset.split(":");
        Integer sunsetHour = Integer.parseInt(parts[0]);
        Integer sunsetMinute = Integer.parseInt(parts[1]);

        String[] parts2 = sunrise.split(":");
        Integer sunriseHour = Integer.parseInt(parts2[0]);
        Integer sunriseMinute = Integer.parseInt(parts2[1]);

        if((hour < sunsetHour || (hour.equals(sunsetHour) && minute<sunsetMinute))&&
                (hour > sunriseHour || (hour.equals(sunriseHour) && minute>sunriseMinute))){
            switch(weatherModel.getCondition()){
                case "Mostly Cloudy":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.cloudsq);
                    break;
                case "Cloudy":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.cloudsq);
                    break;
                case "Foggy":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.cloudsq);
                    break;
                case "Overcast":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.cloudsq);
                    break;
                case "Partly Cloudy":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.cloudsq);
                    break;
                case "Clear":
                    nestedScrollView.setBackgroundResource(R.mipmap.sunnydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.sunshinesq);
                    break;
                case "Sunny":
                    nestedScrollView.setBackgroundResource(R.mipmap.sunnydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.sunshinesq);
                    break;
                case "Rain":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                    break;
                case "Chance of Rain":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                    break;
                case "Showers":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                    break;
                case "Few Showers":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                case "Snow":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                    break;
                case "Snow Showers":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                    break;
                case "Light Rain":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                    break;
                default:
                    nestedScrollView.setBackgroundResource(R.mipmap.sunnydaypicture);
                    gifTextView.setBackgroundResource(R.mipmap.sunshinesq);
            }
        }
        else {
            switch (weatherModel.getCondition()) {
                case "Mostly Cloudy":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.cloudsq);
                    break;
                case "Cloudy":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.cloudsq);
                    break;
                case "Foggy":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.cloudsq);
                    break;
                case "Overcast":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.cloudsq);
                    break;
                case "Partly Cloudy":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.cloudsq);
                    break;
                case "Clear":
                    nestedScrollView.setBackgroundResource(R.mipmap.sunnynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.moonsq);
                    break;
                case "Sunny":
                    nestedScrollView.setBackgroundResource(R.mipmap.sunnynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.moonsq);
                    break;
                case "Rain":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                case "Chance of Rain":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                    break;
                case "Showers":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                    break;
                case "Few Showers":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                case "Snow":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                    break;
                case "Snow Showers":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                    break;
                case "Light Rain":
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                    break;
                default:
                    nestedScrollView.setBackgroundResource(R.mipmap.cloudynightpicture);
                    gifTextView.setBackgroundResource(R.mipmap.rainsq);
                    break;
            }
        }

        listOfHourly = weatherModel.getWeatherHourly();
        // Get the RecyclerView instance
        RecyclerView hourlyRecyclerView = findViewById(R.id.hourlyRecyclerView);

        // Set the LayoutManager to be vertical (default)
        hourlyRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        // Set the adapter which will fill the data on the RecyclerView items
        hourlyRecyclerView.setAdapter(new HourlyRecyclerViewAdaptor(listOfHourly, sunrise, sunset));

        forecasts = weatherModel.getForecasts();
        // Get the RecyclerView instance
        RecyclerView forecastsRecyclerView = findViewById(R.id.ForecastRecyclerView);

        // Set the LayoutManager to be vertical (default)
        forecastsRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));

        // Set the adapter which will fill the data on the RecyclerView items
        forecastsRecyclerView.setAdapter(new ForecastsRecyclerViewAdaptor(forecasts));
    }
}
