package com.example.steve.mobile_assisstant.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Steve on 2018-03-20.
 */

public class HourlyWeatherHolder extends RecyclerView.ViewHolder{
    public TextView hourlyTime;
    public TextView hourlyTemperature;
    public ImageView hourlyPicture;
    public HourlyWeatherHolder(View itemView) {
        super(itemView);
    }
}
