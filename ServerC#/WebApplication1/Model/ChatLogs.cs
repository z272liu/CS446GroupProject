﻿using System;
using System.Collections.Generic;

namespace WebApplication1.Model
{
    public partial class ChatLogs
    {
        public int UniqueId { get; set; }
        public DateTime TimeSent { get; set; }
        public bool Sender { get; set; }
        public string Content { get; set; }
        public int Id { get; set; }

        public UserName Unique { get; set; }
    }
}
