﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

using WebApplication1.Model;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class WeatherController : Controller
    {
        const string UrlBase = "http://api.wunderground.com/api/bed079a6fb121f9c/";
        const string hourly = "hourly/q/";
        const string condition = "conditions/q/";
        const string forcast = "forecast10day/q/";
        private readonly IWeatherService<WeatherHourly[]> _hourlyService;
        private readonly IWeatherService<WeatherServiceModel> _conditionService;
        private readonly IWeatherService<WeatherForcastServiceModel> _forcastService;
        private readonly IWeatherService<WeatherAstronomyServiceModel> _astronomyService;
        private readonly DB_A328BB_z272liuContext _context;
        public WeatherController(DB_A328BB_z272liuContext context, IWeatherService<WeatherHourly[]> hourlyService, 
            IWeatherService<WeatherServiceModel> conditionService,
            IWeatherService<WeatherForcastServiceModel> forcastService,
            IWeatherService<WeatherAstronomyServiceModel> astronomyService) {
            _context = context;
            _hourlyService = hourlyService;
            _conditionService = conditionService;
            _forcastService = forcastService;
            _astronomyService = astronomyService;
        }
        // GET: api/Weather
        [HttpGet]
        public ActionResult Get(string Longitude, string Latitude)
        {
            try
            {
                string parameters = $"{Latitude},{Longitude}.json";
                var conditionData = _conditionService.GetCityData(String.Empty, Longitude, Latitude);
                string cityFullName = conditionData.CityFull;
                var hourlyData = _hourlyService.GetCityData(cityFullName, Longitude, Latitude);
                var forcastData = _forcastService.GetCityData(cityFullName, Longitude, Latitude);
                var astronomyData = _astronomyService.GetCityData(cityFullName, Longitude, Latitude);
                WeatherModel wm = new WeatherModel()
                {
                    Location = conditionData.Location,
                    WindSpeed = conditionData.WindSpeed,
                    Temperature = conditionData.Temperature,
                    MaxTemperature = forcastData.MaxTemperature,
                    MinTemperature = forcastData.MinTemperature,
                    Condition = conditionData.Condition,
                    FeelsLike = conditionData.FeelsLike,
                    Sunrise = astronomyData.Sunrise,
                    Sunset = astronomyData.Sunset,
                    Humidity = conditionData.Humidity,
                    WeatherHourly = hourlyData,
                    Forecasts = forcastData.Forecasts
                };
                ClothModel cm = new ClothModel();
                FoodModel fm = new FoodModel();
                ActivityModel am = GetActivityModel(wm.WeatherHourly);
                var result = _context.Set<ClothDBResult>().FromSql($"GetWeatherRange {Convert.ToDouble(wm.FeelsLike)}").ToList();
                var resultFood = _context.Set<ClothDBResult>().FromSql($"GetWeatherRangeFood {Convert.ToDouble(wm.FeelsLike)}").ToList();
                if (result.Any())
                {
                    cm.ClothSuggestion = new List<Suggestion>();
                    cm.ClothDescription = result.First().Description;
                    foreach (var r in result)
                    {
                        cm.ClothSuggestion.Add(new Suggestion() { Name = r.Name, URL = r.URL });
                    }
                }
                if (resultFood.Any()) {

                    fm.FoodSuggestion = new List<Suggestion>();
                    fm.FoodDescription = resultFood.First().Description;
                    foreach (var r in resultFood)
                    {
                        fm.FoodSuggestion.Add(new Suggestion() { Name = r.Name, URL = r.URL });
                    }
                }
                

                return Json(new WeatherBase() {WeatherModel = wm, ClothModel=cm, FoodModel=fm, ActivityModel=am });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        private int rainInNextHour(WeatherHourly[] weatherHourlies)
        {
            for(int i = 0; i < weatherHourlies.Count(); i++)
            {
                if (weatherHourlies[i].Condition.Contains("Rain") || weatherHourlies[i].Condition.Contains("rain") || weatherHourlies[i].Condition.Contains("Showers"))
                    return i;
            }
            return -1;
        }
        private ActivityModel GetActivityModel(WeatherHourly[] weatherHourlies)
        {
            ActivityModel am = new ActivityModel();
            am.ActivitySuggestion = new List<Suggestion>();
            int i = rainInNextHour(weatherHourlies);
            if (i <= 0)
            {
                am.ActivityDescription = "It will be sunny for the following 12 hours. Outdoor activity is suggested";
                am.ActivitySuggestion.Add(new Suggestion()
                {
                    Name = "Ourdoor Activity",
                    URL = "https://www.fitbook.de/data/uploads/2017/11/gettyimages-200439021-001_1510848032-1040x690.jpg"
                });
                am.ActivitySuggestion.Add(new Suggestion()
                {
                    Name = "Skiing",
                    URL = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-twDPspE_K6GMjBSpnJ9qSzxDRzOESKuRkYvg-pyE4GsZJK6L"
                });
                am.ActivitySuggestion.Add(new Suggestion()
                {
                    Name = "Jogging",
                    URL = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHTRF9FUKkn8hi4jgPJ8GnfudjwxpBkAXL2k5b-UaNfb-0CcRKtg"
                });
            }
            else
            {
                am.ActivityDescription = $"It will be rainy in {i} hours. Please carefully schedule your activites";
                am.ActivitySuggestion.Add(new Suggestion()
                {
                    Name = "Indoor Activity",
                    URL = "https://thumbs.dreamstime.com/z/club-indoor-activity-snooker-pool-bowling-chess-vi-21852095.jpg"
                });
                am.ActivitySuggestion.Add(new Suggestion()
                {
                    Name = "Board Game",
                    URL = "https://images-na.ssl-images-amazon.com/images/I/91D-zq7R8NL._SY550_.jpg"
                });
                am.ActivitySuggestion.Add(new Suggestion()
                {
                    Name = "Room Escape",
                    URL = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTaUdmAlB-v7XRfCwPG9SgPN5xf_KGYuehDfsNgsZTb30Qorf7i"
                });
                am.ActivitySuggestion.Add(new Suggestion()
                {
                    Name = "Video Game",
                    URL = "https://static.businessinsider.com/image/55786ec3eab8ea7d4747eb9d/image.jpg"
                });
            }
            return am;
        }
    }
}
