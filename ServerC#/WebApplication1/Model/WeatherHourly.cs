﻿namespace WebApplication1.Model
{
    public class WeatherHourly
    {
        public string Time { get; set; }
        public string Temperature { get; set; }
        public string Condition { get; set; }
    }
}