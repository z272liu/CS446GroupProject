﻿using System;
using System.Collections.Generic;

namespace WebApplication1.Model
{
    public partial class UserName
    {
        public UserName()
        {
            ChatLogs = new HashSet<ChatLogs>();
        }

        public int UniqueId { get; set; }
        public string Name { get; set; }

        public ICollection<ChatLogs> ChatLogs { get; set; }
    }
}
