package com.example.steve.mobile_assisstant.activity.FoodActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.steve.mobile_assisstant.R;
import com.example.steve.mobile_assisstant.model.ServerModel;
import com.example.steve.mobile_assisstant.model.Suggestion;
import com.google.gson.Gson;

import java.util.List;

public class FoodActivity extends AppCompatActivity {
    ServerModel serverModel;
    String foodDescription;
    List<Suggestion> foodSuggestion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloth);
        Gson gson = new Gson();
        String foodModelStr = getIntent().getStringExtra("ServerModel");

        //cleanData();
        serverModel = gson.fromJson(foodModelStr, ServerModel.class);
        foodDescription = serverModel.getFoodDescription();
        foodSuggestion = serverModel.getFoodSuggestion();
        TextView foodoverall = findViewById(R.id.ClothOverallTxt);
        foodoverall.setText(foodDescription);
        LinearLayout main= findViewById(R.id.SuggestionList);
        for (int i = 0 ; i < foodSuggestion.size(); i+=2) {
            LinearLayout row = new LinearLayout(this);
            //row.setLayoutParams( new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
            row.setOrientation(LinearLayout.HORIZONTAL);
            for(int j = 0; j <= 1; j++){
                int index = i + j;
                if(index < foodSuggestion.size()){
                    LinearLayout layout = new LinearLayout(this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    ImageView im = new ImageView(this);
                    TextView txt = new TextView(this);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(600,600);
                    params.setMargins(10,0,10,0);
                    im.setLayoutParams(params);
                    txt.setText(foodSuggestion.get(index).getName());
                    Glide.with(getApplicationContext()).load(foodSuggestion.get(index).getUrl()).into(im);
                    layout.addView(im);
                    layout.addView(txt);
                    row.addView(layout);
                }
            }
            main.addView(row);
        }
    }

}
