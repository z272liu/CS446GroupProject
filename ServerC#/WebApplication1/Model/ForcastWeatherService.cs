﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace WebApplication1.Model
{
    public class ForcastWeatherService : IWeatherService<WeatherForcastServiceModel>
    {
        const string UrlBase = "http://api.wunderground.com/api/bed079a6fb121f9c/forecast10day/q/";

        public WeatherForcastServiceModel GetCityData(string City, string Longitude, string Latitude)
        {
            string parameters = $"{Latitude},{Longitude}.json";
            using (HttpClient client = new HttpClient())
            {
                var httpRequest = new HttpRequestMessage(HttpMethod.Get, UrlBase + parameters);
                var response = client.SendAsync(httpRequest).Result;
                response.EnsureSuccessStatusCode();
                var result = response.Content.ReadAsStringAsync().Result;
                dynamic jsonObj = JsonConvert.DeserializeObject(result);
                ForecastModel[] forecasts = new ForecastModel[9];
                for(int i = 1; i <= 9; i++)
                {
                    forecasts[i - 1] = new ForecastModel();
                    forecasts[i - 1].Condition = jsonObj.forecast.simpleforecast.forecastday[i].conditions;
                    forecasts[i - 1].DateFull = jsonObj.forecast.simpleforecast.forecastday[i].date.weekday;
                    forecasts[i - 1].TempLow = jsonObj.forecast.simpleforecast.forecastday[i].low.celsius;
                    forecasts[i - 1].TempHigh = jsonObj.forecast.simpleforecast.forecastday[i].high.celsius;
                }
                return new WeatherForcastServiceModel()
                {
                    MaxTemperature = jsonObj.forecast.simpleforecast.forecastday[0].high.celsius,
                    MinTemperature = jsonObj.forecast.simpleforecast.forecastday[0].low.celsius,
                    Forecasts = forecasts
                };
            }
        }
    }
}