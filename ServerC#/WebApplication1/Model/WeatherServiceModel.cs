﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Model
{
    public class WeatherServiceModel
    {
        public string Location { get; set; }
        public string CityFull { get; set; }
        public string Temperature { get; set; }
        public string WindSpeed { get; set; }
        public string FeelsLike { get; set; }
        public string Humidity { get; set; }
        public string Condition { get; set; }
    }
}