﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Model
{
    public class FoodModel
    {
        public string FoodDescription { get; set; }
        public List<Suggestion> FoodSuggestion { get; set; }
    }
}