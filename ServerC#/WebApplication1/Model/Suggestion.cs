﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Model
{
    public class Suggestion
    {
        public string Name { get; set; }
        public string URL { get; set; }

    }
}