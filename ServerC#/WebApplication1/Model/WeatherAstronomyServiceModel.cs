﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Model
{
    public class WeatherAstronomyServiceModel
    {
        public string Sunrise { get; set; }
        public string Sunset { get; set; }
    }
}
