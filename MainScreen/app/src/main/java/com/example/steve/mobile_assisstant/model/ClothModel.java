package com.example.steve.mobile_assisstant.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Alex on 2018/3/14.
 */

public class ClothModel {
    @SerializedName("ClothDescription")
    @Expose
    private String description;
    @SerializedName("ClothSuggestion")
    @Expose
    private List<Suggestion> suggestion;

    public String getDescription() {
        return description;
    }
    public List<Suggestion> getSuggestion() {
        return suggestion;
    }
}
