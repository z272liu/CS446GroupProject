package com.example.steve.mobile_assisstant.model;

import android.os.Environment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.security.Key;
import java.util.HashMap;

/**
 * Created by z272l on 3/26/2018.
 */

public class MapWriter {
    private File imageMappingFile;
    private String imageMappingFileName;
    private String subDir;
    private HashMap<String,String> imageMap;
    public MapWriter(String fileName, String subDirName){
        imageMappingFileName = fileName;
        subDir = subDirName;
        loadImageMappingFile();
    }
    public void cleanData() {
        imageMappingFile.delete();
        /*try {
            imageMappingFile.createNewFile();
            imageMap.clear();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
    private void loadImageMappingFile(){

        File dir = new File(Environment.getExternalStorageDirectory(), subDir);
        //File dir = new File(activity.getFilesDir(), subDir);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        imageMappingFile = new File(dir, imageMappingFileName);
        if (!imageMappingFile.exists()) {
            try {
                imageMappingFile.createNewFile();
                imageMap = new HashMap<>();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            readFromMappingFile();
        }
    }

    private void readFromMappingFile(){
        FileInputStream fis = null;
        InputStreamReader isr = null;
        JsonReader jr = null;
        try {
            fis = new FileInputStream(imageMappingFile);
            isr = new InputStreamReader(fis);
            jr = new JsonReader(isr);
            imageMap = new Gson().fromJson(jr, new TypeToken<HashMap<String,String>>(){}.getType());
            if(imageMap == null)
                imageMap = new HashMap<>();
            jr.close();
            isr.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (jr != null) {
                    jr.close();
                }
                if (isr != null) {
                    isr.close();
                }
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void writeToMappingFile(){
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        JsonWriter jw = null;
        try {
            fos = new FileOutputStream(imageMappingFile);
            osw = new OutputStreamWriter(fos, "UTF-8");
            jw = new JsonWriter(osw);
            new Gson().toJson(imageMap, new TypeToken<HashMap<String,String>>(){}.getType(), jw);
            jw.close();
            osw.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (jw != null) {
                    jw.close();
                }
                if (osw != null) {
                    osw.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String Get(String key){
        return imageMap.get(key);
    }
    public boolean Exist(String Key){
        return imageMap.containsKey(Key);
    }
    public void Set(String Key, String Uri){
        imageMap.put(Key,Uri);
        writeToMappingFile();
    }
}
