package com.example.steve.mobile_assisstant.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Steve on 2018-02-23.
 */

public class WeatherModel {
    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("Temperature")
    @Expose
    private String temperature;
    @SerializedName("MaxTemperature")
    @Expose
    private String maxTemperature;
    @SerializedName("MinTemperature")
    @Expose
    private String minTemperature;
    @SerializedName("WindSpeed")
    @Expose
    private String windSpeed;
    @SerializedName("FeelsLike")
    @Expose
    private String feelsLike;
    @SerializedName("Humidity")
    @Expose
    private String humidity;
    @SerializedName("Condition")
    @Expose
    private String condition;
    @SerializedName("WeatherHourly")
    @Expose
    private List<HourWeather> weatherHourly;
    @SerializedName("Forecasts")
    @Expose
    private List<SimpleWeatherModel> forecasts;
    @SerializedName("Sunrise")
    @Expose
    private String sunrise;
    @SerializedName("Sunset")
    @Expose
    private String sunset;

    public String getLocation() {
        return location;
    }
    public String getTemperature() {
        return temperature;
    }
    public String getMaxTemperature() {
        return maxTemperature;
    }
    public String getMinTemperature() {
        return minTemperature;
    }
    public String getWindSpeed() {
        return windSpeed;
    }
    public String getHumidity() {
        return humidity;
    }
    public String getCondition() {
        return condition;
    }
    public String getFeelsLike() {
        return feelsLike;
    }
    public String getSunrise() {
        return sunrise;
    }
    public String getSunset() {
        return sunset;
    }
    public List<HourWeather> getWeatherHourly() {
        return weatherHourly;
    }
    public List<SimpleWeatherModel> getForecasts() {
        return forecasts;
    }
}


