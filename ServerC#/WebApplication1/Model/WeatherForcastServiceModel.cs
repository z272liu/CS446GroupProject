﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Model
{
    public class WeatherForcastServiceModel
    {
        public string MaxTemperature { get; set; }
        public string MinTemperature { get; set; }
        public ForecastModel[] Forecasts { get; set; }
    }
}