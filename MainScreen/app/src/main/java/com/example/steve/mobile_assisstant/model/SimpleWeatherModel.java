package com.example.steve.mobile_assisstant.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Steve on 2018-03-24.
 */

public class SimpleWeatherModel {
    @SerializedName("DateFull")
    @Expose
    private String dateFull;
    @SerializedName("TempHigh")
    @Expose
    private String tempHigh;
    @SerializedName("TempLow")
    @Expose
    private String tempLow;
    @SerializedName("Condition")
    @Expose
    private String condition;

    public String getDateFull() {
        return dateFull;
    }
    public String getTempHigh() {
        return tempHigh;
    }
    public String getTempLow() {
        return tempLow;
    }
    public String getCondition() {
        return condition;
    }
}
