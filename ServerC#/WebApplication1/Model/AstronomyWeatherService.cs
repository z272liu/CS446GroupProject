﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebApplication1.Model
{
    public class AstronomyWeatherService : IWeatherService<WeatherAstronomyServiceModel>
    {
        const string UrlBase = "http://api.wunderground.com/api/bed079a6fb121f9c/astronomy/q/";

        public WeatherAstronomyServiceModel GetCityData(string City, string Longitude, string Latitude)
        {
            string parameters = $"{Latitude},{Longitude}.json";
            using (HttpClient client = new HttpClient())
            {
                var httpRequest = new HttpRequestMessage(HttpMethod.Get, UrlBase + parameters);
                var response = client.SendAsync(httpRequest).Result;
                response.EnsureSuccessStatusCode();
                var result = response.Content.ReadAsStringAsync().Result;
                dynamic jsonObj = JsonConvert.DeserializeObject(result);
                return new WeatherAstronomyServiceModel()
                {
                    Sunrise = jsonObj.moon_phase.sunrise.hour + ":" + jsonObj.moon_phase.sunrise.minute,
                    Sunset = jsonObj.moon_phase.sunset.hour + ":" + jsonObj.moon_phase.sunset.minute
                };
            }
        }
    }
}
