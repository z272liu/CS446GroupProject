﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Extensions.Caching.Memory;

namespace WebApplication1.Model
{
    public class WeatherServiceCache<T> : IWeatherService<T>
    {
        private IMemoryCache cache;
        private IWeatherService<T> component;
        private int cacheTimeInMinutes;
        public WeatherServiceCache(int cacheTimeInMinutes, IWeatherService<T> weatherService)
        {
            this.cacheTimeInMinutes = cacheTimeInMinutes;
            this.component = weatherService;
            this.cache = new MemoryCache(new MemoryCacheOptions());
        }
        public T GetCityData(string City, string Longitude, string Latitude)
        {
            var cachedObject = cache.Get<T>(City);
            if (cachedObject == null)
            {
                DateTimeOffset absoluteExpiration = DateTimeOffset.Now.AddMinutes(cacheTimeInMinutes);
                cachedObject = component.GetCityData(City, Longitude, Latitude);
                cache.Set<T>(City, cachedObject, absoluteExpiration);
            }
            return cachedObject;
        }
    }
}