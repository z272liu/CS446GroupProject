﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Model
{
    public class ForecastModel
    {
        public string Condition { get; set; }
        public string TempLow { get; set; }
        public string TempHigh { get; set; }
        public string DateFull { get; set; }
    }
}
