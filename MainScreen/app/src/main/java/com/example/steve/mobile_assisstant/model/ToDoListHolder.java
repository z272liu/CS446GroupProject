package com.example.steve.mobile_assisstant.model;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.steve.mobile_assisstant.activity.TodoListSetActivity.Todo_List_Set;
import com.google.gson.Gson;

/**
 * Created by Steve on 2018-02-22.
 */

public class ToDoListHolder extends RecyclerView.ViewHolder {
    public TextView toDoTime;
    public TextView toDoContent;
    public ToDoListHolder(View itemView, final AppCompatActivity activity, final ToDoList toDoList) {
        super(itemView);
        // on item click
        itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                // get position
                Integer pos = getAdapterPosition();
                Intent intent = new Intent(activity,Todo_List_Set.class);
                intent.putExtra("Type", "UPDATE");
                intent.putExtra("Pos", pos.toString());
                intent.putExtra("Data", gson.toJson(toDoList.getEvent(pos)));
                activity.startActivityForResult(intent,4);
            }
        });
    }
}
