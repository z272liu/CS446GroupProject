package com.example.steve.mobile_assisstant.model;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;

/**
 * Created by z272l on 3/26/2018.
 */

public class LocalImageProcessor implements ImageProcessorStrategy {
    private String _uri;
    private final int borderSize = 5;
    private final int width = 170;
    private final int height = 170;
    private Context _context;
    public LocalImageProcessor(String uri, Context context){
        _uri = uri;
        _context = context;
    }
    @Override
    public Bitmap GetDesiredImage(){
        try {
            Bitmap myBitmap = Bitmap.createScaledBitmap(MediaStore.Images.Media.getBitmap(_context.getContentResolver(), Uri.parse( _uri)),width,height,false);
            Bitmap bmpWithBorder = Bitmap.createBitmap(myBitmap.getWidth() + borderSize * 2, myBitmap.getHeight() + borderSize * 2, myBitmap.getConfig());
            Canvas canvas = new Canvas(bmpWithBorder);
            canvas.drawColor(Color.BLUE);
            canvas.drawBitmap(myBitmap, borderSize, borderSize, null);
            return bmpWithBorder;
        } catch (Exception e) {
            // Log exception
            return null;
        }
    }
    @Override
    public String GetDescription(){
        return "hello";
    }
}
