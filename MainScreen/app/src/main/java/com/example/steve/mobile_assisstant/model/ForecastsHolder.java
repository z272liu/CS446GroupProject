package com.example.steve.mobile_assisstant.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Steve on 2018-03-24.
 */

public class ForecastsHolder extends RecyclerView.ViewHolder {
    public TextView forecastWeekday;
    public TextView forecastTemperature;
    public ImageView forecastPicture;
    public ForecastsHolder(View itemView) {
        super(itemView);
    }
}
