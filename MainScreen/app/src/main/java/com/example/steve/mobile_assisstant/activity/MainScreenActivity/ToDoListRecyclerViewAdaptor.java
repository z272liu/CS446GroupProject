package com.example.steve.mobile_assisstant.activity.MainScreenActivity;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.steve.mobile_assisstant.R;
import com.example.steve.mobile_assisstant.model.ToDoList;
import com.example.steve.mobile_assisstant.model.ToDoListHolder;

import java.text.SimpleDateFormat;

/**
 * Created by Steve on 2018-02-22.
 */

public class ToDoListRecyclerViewAdaptor extends RecyclerView.Adapter<ToDoListHolder> {
    private AppCompatActivity activity;
    private ToDoList toDoList;

    public ToDoListRecyclerViewAdaptor(AppCompatActivity activity, ToDoList toDoList){
        this.activity = activity;
        this.toDoList = toDoList;
    }

    @Override
    public ToDoListHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Create a new view
        ConstraintLayout repoItemView = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_user_todo, parent, false);

        // Get references to view components
        TextView content = repoItemView.findViewById(R.id.todo_content);
        TextView time = repoItemView.findViewById(R.id.todo_time);

        // Create a new ViewHolder instance and attach references to it
        ToDoListHolder viewHolder = new ToDoListHolder(repoItemView, activity, toDoList);
        viewHolder.toDoTime = time;
        viewHolder.toDoContent = content;

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ToDoListHolder holder, int position) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
        String folderName = formatter.format(toDoList.getEvent(position).getDate());
        holder.toDoTime.setText(folderName);
        holder.toDoContent.setText(toDoList.getEvent(position).getName());
    }

    @Override
    public int getItemCount() {
        return toDoList.getSize();
    }
}
