package com.example.steve.mobile_assisstant.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Steve on 2018-02-23.
 */

public class FoodModel {
    @SerializedName("FoodDescription")
    @Expose
    private String description;
    @SerializedName("FoodSuggestion")
    @Expose
    private List<Suggestion> suggestion;

    public String getDescription() {
        return description;
    }
    public List<Suggestion> getSuggestion() {
        return suggestion;
    }
}
