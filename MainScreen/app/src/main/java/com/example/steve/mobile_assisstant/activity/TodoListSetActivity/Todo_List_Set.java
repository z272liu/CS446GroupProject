package com.example.steve.mobile_assisstant.activity.TodoListSetActivity;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.steve.mobile_assisstant.R;
import com.example.steve.mobile_assisstant.model.ToDoEvent;
import com.example.steve.mobile_assisstant.model.ToDoList;
import com.example.steve.mobile_assisstant.model.ToDoListHolder;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

public class Todo_List_Set extends AppCompatActivity {
    private String type;
    private String pos;
    private ToDoEvent toDoEvent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo__list__set);
        TextView textView = (TextView) findViewById(R.id.ToDo_Time);
        textView.setText("Choose Time");
    }

    @Override
    protected void onStart() {
        Gson gson = new Gson();
        super.onStart();
        type= getIntent().getStringExtra("Type");
        Button button = findViewById(R.id.button5);
        if(type.equals("ADD")){
            button.setVisibility(View.GONE);
        }
        else{
            pos = getIntent().getStringExtra("Pos");
            button.setVisibility(View.VISIBLE);
            String data = getIntent().getStringExtra("Data");
            toDoEvent = gson.fromJson(data,ToDoEvent.class);
            EditText toDo_Input = findViewById(R.id.Todo_Input);
            //EditText toDo_BeforeNUm = findViewById(R.id.ToDo_Before_Num);
            TextView toDo_Time = findViewById(R.id.ToDo_Time);
            EditText toDo_Detail = findViewById(R.id.ToDo_Detail);
            toDo_Input.setText(toDoEvent.getName());
            /*Date time =toDoEvent.getAlarm();
            if(time != null){
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(time);
                Integer before = calendar.get(Calendar.MINUTE);
                toDo_BeforeNUm.setText(before.toString());
            }*/
            toDo_Detail.setText(toDoEvent.getDescription());
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
            String folderName = formatter.format(toDoEvent.getDate());
            toDo_Time.setText(folderName);
        }
    }

    public void changeVisibility(View view) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.PickerLayout);
        SingleDateAndTimePicker singleDateAndTimePicker=
                (SingleDateAndTimePicker) findViewById(R.id.DateTimePicker);
        if(linearLayout.getVisibility()==View.VISIBLE) {
            linearLayout.setVisibility(View.GONE);
        }
        else{
            linearLayout.setVisibility(View.VISIBLE);
        }

        TextView textView = (TextView) findViewById(R.id.ToDo_Time);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
        String folderName = formatter.format(singleDateAndTimePicker.getDate());
        textView.setText(folderName);
    }

    public void submitButton(View view){
        EditText toDo_Input = findViewById(R.id.Todo_Input);
        //EditText toDo_BeforeNUm = findViewById(R.id.ToDo_Before_Num);
        SingleDateAndTimePicker singleDateAndTimePicker=
                (SingleDateAndTimePicker) findViewById(R.id.DateTimePicker);
        EditText toDo_Detail = findViewById(R.id.ToDo_Detail);

        String input    = toDo_Input.getText().toString();

        Integer before;
        Date time       = singleDateAndTimePicker.getDate();
        if(type.equals("UPDATE")){
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm");
            String temp1 = formatter.format(singleDateAndTimePicker.getDate());
            String temp2      = ((TextView)findViewById(R.id.ToDo_Time)).getText().toString();
            if(!Objects.equals(temp1, temp2)){
                time = toDoEvent.getDate();
            }
        }
        String detail = toDo_Detail.getText().toString();
        ToDoEvent temptoDoEvent;
        /*Date beforeDate;
        if(toDo_BeforeNUm.getText().toString().equals("")){
            beforeDate = null;
        }
        else{
            before= Integer.parseInt(toDo_BeforeNUm.getText().toString());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(time);
            calendar.add(Calendar.MINUTE, before);
            beforeDate = calendar.getTime();
        }*/
        temptoDoEvent =new ToDoEvent(time,input,detail,null);
        Intent i = new Intent();
        Gson gson = new Gson();
        i.putExtra("ToDoEvent", gson.toJson(temptoDoEvent));
        if (type.equals("UPDATE")) {
            i.putExtra("Pos", pos);
            setResult(5,i);
        }
        else {
            setResult(4, i);
        }
        finish();
    }

    public void delete(View view){
        Intent i = new Intent();
        i.putExtra("Pos", pos);
        setResult(6,i);
        finish();
    }
}
