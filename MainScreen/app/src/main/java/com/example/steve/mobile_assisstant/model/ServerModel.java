package com.example.steve.mobile_assisstant.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Alex on 2018/3/14.
 */

public class ServerModel {
    @SerializedName("WeatherModel")
    @Expose
    private WeatherModel weatherModel;
    @SerializedName("ClothModel")
    @Expose
    private ClothModel clothModel;
    @SerializedName("FoodModel")
    @Expose
    private FoodModel foodModel;
    @SerializedName("ActivityModel")
    @Expose
    private ActivityModel activityModel;

    /*
    // need to be deleted by steve
    public WeatherModel getWeatherModel() {
        return weatherModel;
    }
    // need to be deleted by steve
    public ClothModel getClothModel() {
        return clothModel;
    }
    // need to be deleted by steve
    public FoodModel getFoodModel() {
        return foodModel;
    }
    // need to be deleted by steve
    public ActivityModel getActivityModel() {
        return activityModel;
    }
    */

    // function for access WeatherModel
    public String getLocation() {
        return weatherModel.getLocation();
    }
    public String getTemperature() {
        return weatherModel.getTemperature();
    }
    public String getMaxTemperature() {
        return weatherModel.getMaxTemperature();
    }
    public String getMinTemperature() {
        return weatherModel.getMinTemperature();
    }
    public String getWindSpeed() {
        return weatherModel.getWindSpeed();
    }
    public String getHumidity() {
        return weatherModel.getHumidity();
    }
    public String getCondition() {
        return weatherModel.getCondition();
    }
    public String getFeelsLike() {
        return weatherModel.getFeelsLike();
    }
    public String getSunrise() {
        return weatherModel.getSunrise();
    }
    public String getSunset() {
        return weatherModel.getSunset();
    }
    public List<HourWeather> getWeatherHourly() {
        return weatherModel.getWeatherHourly();
    }
    public List<SimpleWeatherModel> getForecasts() {
        return weatherModel.getForecasts();
    }

    // function for access ClothModel
    public String getClothDescription() {
        return clothModel.getDescription();
    }
    public List<Suggestion> getClothSuggestion() {
        return clothModel.getSuggestion();
    }

    // function for access FoodModel
    public String getFoodDescription() {
        return foodModel.getDescription();
    }
    public List<Suggestion> getFoodSuggestion() {
        return foodModel.getSuggestion();
    }

    // function for access ActivityModel
    public String getActivityDescription() {
        return activityModel.getDescription();
    }
    public List<Suggestion> getActivitySuggestion() {
        return activityModel.getSuggestion();
    }
}
