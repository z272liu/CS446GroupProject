﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApplication1.Model
{
    public partial class DB_A328BB_z272liuContext : DbContext
    {
        public virtual DbSet<ClothDBResult> ClothDBResult { get; set; }
        public virtual DbSet<ChatLogs> ChatLogs { get; set; }
        public virtual DbSet<UserName> UserName { get; set; }
        public DB_A328BB_z272liuContext(DbContextOptions<DB_A328BB_z272liuContext> options)
    : base(options)
{ }
        // Unable to generate entity type for table 'dbo.ClothRange'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Combination'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.Cloth'. Please see the warning messages.


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChatLogs>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.TimeSent).HasColumnType("datetime");

                entity.Property(e => e.UniqueId).HasColumnName("UniqueID");

                entity.HasOne(d => d.Unique)
                    .WithMany(p => p.ChatLogs)
                    .HasForeignKey(d => d.UniqueId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UniqueID");
            });

            modelBuilder.Entity<UserName>(entity =>
            {
                entity.HasKey(e => e.UniqueId);

                entity.Property(e => e.UniqueId).HasColumnName("UniqueID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });
        }
    }
}
