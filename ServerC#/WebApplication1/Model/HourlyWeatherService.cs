﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication1.Model
{
    public class HourlyWeatherService : IWeatherService<WeatherHourly[]>
    {
        const string UrlBase = "http://api.wunderground.com/api/bed079a6fb121f9c/hourly/q/";
        public WeatherHourly[] GetCityData(string City, string Longitude, string Latitude)
        {
            string parameters = $"{Latitude},{Longitude}.json";
            using (HttpClient client = new HttpClient())
            {
                var httpRequest = new HttpRequestMessage(HttpMethod.Get, UrlBase + parameters);
                var response = client.SendAsync(httpRequest).Result;
                response.EnsureSuccessStatusCode();
                var result = response.Content.ReadAsStringAsync().Result;
                dynamic jsonObj = JsonConvert.DeserializeObject(result);
                WeatherHourly[] weatherHourly = new WeatherHourly[12];
                for(int i = 0; i < 12; i++) {
                    //int j = Convert.ToInt32(jsonObj.hourly_forecast[i].hour) % 12;
                    //string s = Convert.ToString(Convert.ToInt32(jsonObj.hourly_forecast[i].FCTTIME.hour) % 12);
                    WeatherHourly wh = new WeatherHourly()
                    {
                        Time = Convert.ToString(Convert.ToInt32((int)jsonObj.hourly_forecast[i].FCTTIME.hour) % 12) + jsonObj.hourly_forecast[i].FCTTIME.ampm,
                        Temperature = jsonObj.hourly_forecast[i].temp.metric,
                        Condition = jsonObj.hourly_forecast[i].wx
                    };
                    weatherHourly[i] = wh;
                };
                return weatherHourly;
            }
        }
    }
}