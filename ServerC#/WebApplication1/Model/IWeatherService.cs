﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Model
{
    public interface IWeatherService<T>
    {
         T GetCityData(string City, string Longitude, string Latitude);
    }
}
