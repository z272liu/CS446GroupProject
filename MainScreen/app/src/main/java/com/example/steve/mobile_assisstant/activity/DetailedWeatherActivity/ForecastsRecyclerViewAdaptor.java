package com.example.steve.mobile_assisstant.activity.DetailedWeatherActivity;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.steve.mobile_assisstant.R;
import com.example.steve.mobile_assisstant.model.ForecastsHolder;
import com.example.steve.mobile_assisstant.model.HourWeather;
import com.example.steve.mobile_assisstant.model.HourlyWeatherHolder;
import com.example.steve.mobile_assisstant.model.SimpleWeatherModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Steve on 2018-03-24.
 */

public class ForecastsRecyclerViewAdaptor extends RecyclerView.Adapter<ForecastsHolder>{
    private List<SimpleWeatherModel> forecasts = new ArrayList<>();

    public ForecastsRecyclerViewAdaptor(List<SimpleWeatherModel> forecasts) {
        this.forecasts = forecasts;
    }

    @Override
    public ForecastsHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Create a new view
        ConstraintLayout forecastsView = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.forecast_weather, parent, false);

        // Get references to view components
        TextView forecastWeekday = forecastsView.findViewById(R.id.forecast_weekday);
        ImageView forecastPicture = forecastsView.findViewById(R.id.forecast_picture);
        TextView forecastTemperature = forecastsView.findViewById(R.id.forecast_temperature);

        // Create a new ViewHolder instance and attach references to it
        ForecastsHolder viewHolder = new ForecastsHolder(forecastsView);
        viewHolder.forecastWeekday= forecastWeekday;
        viewHolder.forecastPicture = forecastPicture;
        viewHolder.forecastTemperature = forecastTemperature;

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ForecastsHolder holder, int position) {
        SimpleWeatherModel forecastWeather = forecasts.get(position);
        holder.forecastWeekday.setText(forecastWeather.getDateFull());
        holder.forecastTemperature.setText(forecastWeather.getTempHigh()+" / " + forecastWeather.getTempLow());

        switch (forecastWeather.getCondition()) {
            case "Mostly Cloudy":
                holder.forecastPicture.setImageResource(R.mipmap.cloudypicture);
                break;
            case "Cloudy":
                holder.forecastPicture.setImageResource(R.mipmap.cloudypicture);
                break;
            case "Foggy":
                holder.forecastPicture.setImageResource(R.mipmap.cloudypicture);
                break;
            case "Overcast":
                holder.forecastPicture.setImageResource(R.mipmap.cloudypicture);
                break;
            case "Partly Cloudy":
                holder.forecastPicture.setImageResource(R.mipmap.partlycloudypicture);
                break;
            case "Clear":
                holder.forecastPicture.setImageResource(R.mipmap.clearpicture);
                break;
            case "Sunny":
                holder.forecastPicture.setImageResource(R.mipmap.clearpicture);
                break;
            case "Rain":
                holder.forecastPicture.setImageResource(R.mipmap.rainpicture);
                break;
            case "Showers":
                holder.forecastPicture.setImageResource(R.mipmap.rainpicture);
                break;
            case "Chance of Rain":
                holder.forecastPicture.setImageResource(R.mipmap.rainpicture);
                break;
            case "Few Showers":
                holder.forecastPicture.setImageResource(R.mipmap.rainpicture);
                break;
            case "Light Rain":
                holder.forecastPicture.setImageResource(R.mipmap.rainpicture);
                break;
            case "Snow":
                holder.forecastPicture.setImageResource(R.mipmap.snowpicture);
                break;
            case "Snow Showers":
                holder.forecastPicture.setImageResource(R.mipmap.snowpicture);
                break;
            default:
                holder.forecastPicture.setImageResource(R.mipmap.rainpicture);
                break;
        }
    }


    @Override
    public int getItemCount() {
        return forecasts.size();
    }
}
