package com.example.steve.mobile_assisstant.activity.DetailedWeatherActivity;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.steve.mobile_assisstant.R;
import com.example.steve.mobile_assisstant.model.HourWeather;
import com.example.steve.mobile_assisstant.model.HourlyWeatherHolder;
import com.example.steve.mobile_assisstant.model.ServerModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Steve on 2018-03-20.
 */

public class HourlyRecyclerViewAdaptor extends RecyclerView.Adapter<HourlyWeatherHolder> {

    private List<HourWeather> hourWeathers = new ArrayList<>();
    private String sunriseString;
    private String sunsetString;

    public HourlyRecyclerViewAdaptor(List<HourWeather> hourWeathers, String sunriseString, String sunsetString) {
        this.hourWeathers = hourWeathers;
        this.sunriseString =sunriseString;
        this.sunsetString =sunsetString;
    }

    @Override
    public HourlyWeatherHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // Create a new view
        ConstraintLayout hourlyView = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hourly_weather, parent, false);

        // Get references to view components
        TextView hourlyTime = hourlyView.findViewById(R.id.hourlyTime);
        ImageView hourlyPicture = hourlyView.findViewById(R.id.hourly_weather_picture);
        TextView hourlyTemperature = hourlyView.findViewById(R.id.hourlyTemperature);

        // Create a new ViewHolder instance and attach references to it
        HourlyWeatherHolder viewHolder = new HourlyWeatherHolder(hourlyView);
        viewHolder.hourlyTime= hourlyTime;
        viewHolder.hourlyPicture = hourlyPicture;
        viewHolder.hourlyTemperature = hourlyTemperature;

        return viewHolder;

    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(HourlyWeatherHolder holder, int position) {
        HourWeather hourWeather = hourWeathers.get(position);
        holder.hourlyTime.setText(hourWeather.getTime());
        holder.hourlyTemperature.setText(hourWeather.getTemperature()+ "°");

        String sunrise = sunriseString;
        String[] parts = sunrise.split(":");
        Integer sunriseHour = Integer.parseInt(parts[0]);

        String sunset = sunsetString;
        String[] parts2 = sunset.split(":");
        Integer sunsetHour = Integer.parseInt(parts2[0]);

        String temp = hourWeather.getTime();
        Integer temptime = Integer.parseInt(temp.substring(0,temp.length() - 2 ));
        String dayornight = temp.substring(temp.length() - 2 );
        if(dayornight.equals("PM")){
            temptime += 12;
        }

        switch (hourWeather.getCondition()) {
            case "Mostly Cloudy":
                holder.hourlyPicture.setImageResource(R.mipmap.cloudypicture);
                break;
            case "Cloudy":
                holder.hourlyPicture.setImageResource(R.mipmap.cloudypicture);
                break;
            case "Foggy":
                holder.hourlyPicture.setImageResource(R.mipmap.cloudypicture);
                break;
            case "Overcast":
                holder.hourlyPicture.setImageResource(R.mipmap.cloudypicture);
                break;
            case "Partly Cloudy":
                holder.hourlyPicture.setImageResource(R.mipmap.partlycloudypicture);
                break;
            case "Clear":
                holder.hourlyPicture.setImageResource(R.mipmap.clearpicture);
                if(temptime>sunsetHour || temptime<sunriseHour) {
                    holder.hourlyPicture.setImageResource(R.mipmap.moonpicture);
                }
                break;
            case "Sunny":
                holder.hourlyPicture.setImageResource(R.mipmap.clearpicture);
                if(temptime>sunsetHour || temptime<sunriseHour) {
                    holder.hourlyPicture.setImageResource(R.mipmap.moonpicture);
                }
                break;
            case "Rain":
                holder.hourlyPicture.setImageResource(R.mipmap.rainpicture);
                break;
            case "Chance of Rain":
                holder.hourlyPicture.setImageResource(R.mipmap.rainpicture);
                break;
            case "Showers":
                holder.hourlyPicture.setImageResource(R.mipmap.rainpicture);
                break;
            case "Few Showers":
                holder.hourlyPicture.setImageResource(R.mipmap.rainpicture);
                break;
            case "Light Rain":
                holder.hourlyPicture.setImageResource(R.mipmap.rainpicture);
                break;
            default:
                holder.hourlyPicture.setImageResource(R.mipmap.clearpicture);
                if(temptime>sunsetHour || temptime<sunriseHour) {
                    holder.hourlyPicture.setImageResource(R.mipmap.moonpicture);
                }
                break;
        }
    }


    @Override
    public int getItemCount() {
        return hourWeathers.size();
    }
}
