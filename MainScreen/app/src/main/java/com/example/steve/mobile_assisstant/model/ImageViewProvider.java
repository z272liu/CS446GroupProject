package com.example.steve.mobile_assisstant.model;
import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.view.View.OnClickListener;
import android.view.View;

import com.example.steve.mobile_assisstant.activity.ClothActivity.ClothActivity;

/**
 * Created by z272l on 3/26/2018.
 */

public class ImageViewProvider {
    ImageProcessorStrategy _ips;
    public View GetItemView(Suggestion s, int index, Activity activity){

        LinearLayout layout = new LinearLayout(activity.getApplicationContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        ImageView im = new ImageView(activity.getApplicationContext());
        im.setId(index);
        TextView txt = new TextView(activity.getApplicationContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(170,170);
        params.setMargins(10,0,10,0);
        im.setLayoutParams(params);
        txt.setText(s.getName());
        txt.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,10));
        im.setImageBitmap(_ips.GetDesiredImage());
        layout.addView(im);
        layout.addView(txt);
        im.setClickable(true);
        im.setOnClickListener(new ClothActivity.ClothImageClickListener(index, activity));
        return im;
    }
    public void setStrategy( ImageProcessorStrategy ips){
        _ips = ips;
    }
}
