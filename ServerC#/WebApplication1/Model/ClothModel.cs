﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Model
{
    public class ClothModel
    {
        public string ClothDescription { get; set; }
        public List<Suggestion> ClothSuggestion { get; set; }
    }
}