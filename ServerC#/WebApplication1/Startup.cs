﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplication1.Model;
using Microsoft.EntityFrameworkCore;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver
                    = new Newtonsoft.Json.Serialization.DefaultContractResolver();
            });
            string connection = @"Server=SQL5037.site4now.net;Database=DB_A328BB_z272liu;User Id=DB_A328BB_z272liu_admin; Password=Lzh123456#;";
            services.AddDbContext<DB_A328BB_z272liuContext>(options => options.UseSqlServer(connection));
            IWeatherService<WeatherHourly[]> _hourlyService = new HourlyWeatherService();
            IWeatherService<WeatherServiceModel> _conditionService = new ConditionWeatherService();
            IWeatherService<WeatherForcastServiceModel> _forcastService = new ForcastWeatherService();
            IWeatherService<WeatherAstronomyServiceModel> _astronomyService = new AstronomyWeatherService();
            IWeatherService<WeatherHourly[]> _hourlyServiceWithCache = new WeatherServiceCache<WeatherHourly[]>(60, _hourlyService);
            IWeatherService<WeatherForcastServiceModel> _forcastServiceWithCache = new WeatherServiceCache<WeatherForcastServiceModel>(60, _forcastService);
            IWeatherService<WeatherAstronomyServiceModel> _astronomyServiceWithCache = new WeatherServiceCache<WeatherAstronomyServiceModel>(60, _astronomyService);
            services.AddSingleton(typeof(IWeatherService<WeatherHourly[]>), _hourlyServiceWithCache);
            services.AddSingleton(typeof(IWeatherService<WeatherServiceModel>), _conditionService);
            services.AddSingleton(typeof(IWeatherService<WeatherForcastServiceModel>), _forcastServiceWithCache);
            services.AddSingleton(typeof(IWeatherService<WeatherAstronomyServiceModel>), _astronomyServiceWithCache);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
