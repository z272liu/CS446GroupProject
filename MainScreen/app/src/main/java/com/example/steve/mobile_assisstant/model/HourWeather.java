package com.example.steve.mobile_assisstant.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Steve on 2018-02-26.
 */

public class HourWeather {
    @SerializedName("Time")
    @Expose
    private String time;
    @SerializedName("Temperature")
    @Expose
    private String temperature;
    @SerializedName("Condition")
    @Expose
    private String condition;

    public String getTime() {
        return time;
    }
    public String getTemperature() {
        return temperature;
    }
    public String getCondition() {
        return condition;
    }
}
