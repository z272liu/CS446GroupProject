package com.example.steve.mobile_assisstant.activity.MainScreenActivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.example.steve.mobile_assisstant.R;
import com.example.steve.mobile_assisstant.activity.ClothActivity.ClothActivity;
import com.example.steve.mobile_assisstant.activity.DetailedWeatherActivity.DetailedWeather;
import com.example.steve.mobile_assisstant.activity.FoodActivity.FoodActivity;
import com.example.steve.mobile_assisstant.activity.TodoListSetActivity.Todo_List_Set;
import com.example.steve.mobile_assisstant.model.LeoService;
import com.example.steve.mobile_assisstant.model.ServerModel;
import com.example.steve.mobile_assisstant.model.ToDoEvent;
import com.example.steve.mobile_assisstant.model.ToDoList;
import com.example.steve.mobile_assisstant.activity.Activity.ActivityActivity;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import pl.droidsonroids.gif.GifTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    private LocationManager locationManager;
    //private LocationListener locationListener;
    private Location location;
    private ServerModel serverModel;
    private TextView main_Location;
    private TextView main_Temp;
    private TextView main_HLtemp;
    private ToDoList toDoList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        toDoList = new ToDoList();
        //toDoList.cleanData();

        // Get the RecyclerView instance
        RecyclerView reposRecyclerView = findViewById(R.id.toDoListRecyclerView);

        // Set the LayoutManager to be vertical (default)
        reposRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));

        // Set the adapter which will fill the data on the RecyclerView items
        reposRecyclerView.setAdapter(new ToDoListRecyclerViewAdaptor(this, toDoList));

        // match each TextView
        main_Location = (TextView) findViewById(R.id.main_Location);
        main_Temp = (TextView) findViewById(R.id.main_temp);
        main_HLtemp = (TextView) findViewById(R.id.main_HLtemp);

        updateInfo(null);
    }


    public void updateInfo(View view) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            applyPermission();
            return;
        }
        getLocation();
        while (location == null) {
            getLocation();
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://z272liu-001-site1.ctempurl.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        LeoService service = retrofit.create(LeoService.class);
        final Call<ServerModel> serverModelCall = service.getServerModel(
                location.getLongitude(), location.getLatitude());
        serverModelCall.enqueue(new Callback<ServerModel>() {
            @Override
            public void onResponse(Call<ServerModel> call, Response<ServerModel> response) {
                serverModel = response.body();
                setView();
            }

            @Override
            public void onFailure(Call<ServerModel> call, Throwable t) {
                internetDialog();
            }
        });
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            applyPermission();
            return;
        }
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        location = locationManager
                .getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location == null) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, new LocationListener() {
                @Override
                public void onLocationChanged(Location inputLocation) {
                    if (inputLocation != null) {
                        location = inputLocation;
                    }
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {
                }

                @Override
                public void onProviderEnabled(String s) {
                    location = locationManager.getLastKnownLocation(s);
                }

                @Override
                public void onProviderDisabled(String s) {
                }
            });
        }
    }

    private void applyPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            updateInfo(null);
        }
        else if (!shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) || !shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            permissionDialog();
        }
        /*
        else {
            applyPermission();
        }*/
    }

    private void permissionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Need Location Access Permission!");
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.setData(Uri.parse("package:" + getPackageName())); // 根据包名打开对应的设置界面
                startActivity(intent);
            }
        });
        builder.create().show();
    }

    private void internetDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Internet Error");
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                startActivity(intent);
            }
        });
        builder.create().show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Gson gson = new Gson();
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 3 && resultCode == 4) {
            String returnedString = data.getStringExtra("ToDoEvent");
            System.out.println(returnedString);
            ToDoEvent toDoEvent = gson.fromJson(returnedString,ToDoEvent.class);
            toDoList.add(toDoEvent);
            // Get the RecyclerView instance
            RecyclerView reposRecyclerView = findViewById(R.id.toDoListRecyclerView);

            // Set the LayoutManager to be vertical (default)
            reposRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));

            // Set the adapter which will fill the data on the RecyclerView items
            reposRecyclerView.setAdapter(new ToDoListRecyclerViewAdaptor(this, toDoList));
        }
        if (requestCode == 4 && resultCode == 5) {
            String returnedString = data.getStringExtra("ToDoEvent");
            int pos = Integer.parseInt(data.getStringExtra("Pos"));
            System.out.println(returnedString);
            ToDoEvent toDoEvent = gson.fromJson(returnedString,ToDoEvent.class);
            toDoList.set(pos, toDoEvent);
            // Get the RecyclerView instance
            RecyclerView reposRecyclerView = findViewById(R.id.toDoListRecyclerView);

            // Set the LayoutManager to be vertical (default)
            reposRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));

            // Set the adapter which will fill the data on the RecyclerView items
            reposRecyclerView.setAdapter(new ToDoListRecyclerViewAdaptor(this, toDoList));
        }
        if (requestCode == 4 && resultCode == 6) {
            int pos = Integer.parseInt(data.getStringExtra("Pos"));
            toDoList.remove(pos);
            // Get the RecyclerView instance
            RecyclerView reposRecyclerView = findViewById(R.id.toDoListRecyclerView);

            // Set the LayoutManager to be vertical (default)
            reposRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));

            // Set the adapter which will fill the data on the RecyclerView items
            reposRecyclerView.setAdapter(new ToDoListRecyclerViewAdaptor(this, toDoList));
        }
    }

    private void setView() {
        main_Location.setText(serverModel.getLocation());
        main_Temp.setText(serverModel.getTemperature() + "°C");
        main_HLtemp.setText(serverModel.getMaxTemperature() + " / " + serverModel.getMinTemperature());
        ConstraintLayout recyclerView =findViewById(R.id.TodoListPresenter);
        GifTextView gifTextView =findViewById(R.id.main_Temperature_Background);

        Calendar cal = Calendar.getInstance();
        Integer hour    = cal.get(Calendar.HOUR_OF_DAY);
        Integer minute  = cal.get(Calendar.MINUTE);

        String sunrise = serverModel.getSunrise();
        String sunset  = serverModel.getSunset();

        String[] parts = sunset.split(":");
        Integer sunsetHour = Integer.parseInt(parts[0]);
        Integer sunsetMinute = Integer.parseInt(parts[1]);

        String[] parts2 = sunrise.split(":");
        Integer sunriseHour = Integer.parseInt(parts2[0]);
        Integer sunriseMinute = Integer.parseInt(parts2[1]);

        if((hour < sunsetHour || (hour.equals(sunsetHour) && minute<sunsetMinute))&&
                (hour > sunriseHour || (hour.equals(sunriseHour) && minute>sunriseMinute))){
            switch(serverModel.getCondition()){
                case "Mostly Cloudy":
                    gifTextView.setBackgroundResource(R.mipmap.clouddaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudydaypictureflip);
                    break;
                case "Cloudy":
                    gifTextView.setBackgroundResource(R.mipmap.clouddaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudydaypictureflip);
                    break;
                case "Foggy":
                    gifTextView.setBackgroundResource(R.mipmap.clouddaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudydaypictureflip);
                    break;
                case "Overcast":
                    gifTextView.setBackgroundResource(R.mipmap.clouddaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudydaypictureflip);
                    break;
                case "Partly Cloudy":
                    gifTextView.setBackgroundResource(R.mipmap.clouddaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudydaypictureflip);
                    break;
                case "Clear":
                    gifTextView.setBackgroundResource(R.mipmap.sunshinedaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.sunnydaypictureflip);
                    break;
                case "Sunny":
                    gifTextView.setBackgroundResource(R.mipmap.sunshinedaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.sunnydaypictureflip);
                    break;
                case "Rain":
                    gifTextView.setBackgroundResource(R.mipmap.raindaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudydaypictureflip);
                    break;
                case "Showers":
                    gifTextView.setBackgroundResource(R.mipmap.raindaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudydaypictureflip);
                    break;
                case "Snow":
                    gifTextView.setBackgroundResource(R.mipmap.snowdaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudydaypictureflip);
                    break;
                case "Snow Showers":
                    gifTextView.setBackgroundResource(R.mipmap.snowdaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudydaypictureflip);
                    break;
                case "Few Showers":
                    gifTextView.setBackgroundResource(R.mipmap.raindaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudydaypictureflip);
                    break;
                case "Light Rain":
                    gifTextView.setBackgroundResource(R.mipmap.raindaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudydaypictureflip);
                    break;
                case "Chance of Rain":
                    gifTextView.setBackgroundResource(R.mipmap.raindaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudydaypictureflip);
                    break;
                default:
                    gifTextView.setBackgroundResource(R.mipmap.raindaybackground);
                    recyclerView.setBackgroundResource(R.mipmap.darkpictureflip);
            }
        }
        else {
            switch (serverModel.getCondition()) {
                case "Mostly Cloudy":
                    gifTextView.setBackgroundResource(R.mipmap.cloudnightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudynightpictureflip);
                    break;
                case "Cloudy":
                    gifTextView.setBackgroundResource(R.mipmap.cloudnightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudynightpictureflip);
                    break;
                case "Foggy":
                    gifTextView.setBackgroundResource(R.mipmap.cloudnightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudynightpictureflip);
                    break;
                case "Overcast":
                    gifTextView.setBackgroundResource(R.mipmap.cloudnightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudynightpictureflip);
                    break;
                case "Partly Cloudy":
                    gifTextView.setBackgroundResource(R.mipmap.cloudnightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.cloudynightpictureflip);
                    break;
                case "Clear":
                    gifTextView.setBackgroundResource(R.mipmap.moonbackground);
                    recyclerView.setBackgroundResource(R.mipmap.sunnynightpictureflip);
                    break;
                case "Sunny":
                    gifTextView.setBackgroundResource(R.mipmap.moonbackground);
                    recyclerView.setBackgroundResource(R.mipmap.sunnynightpictureflip);
                    break;
                case "Rain":
                    gifTextView.setBackgroundResource(R.mipmap.rainnightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.darkpictureflip);
                    break;
                case "Chance of Rain":
                    gifTextView.setBackgroundResource(R.mipmap.rainnightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.darkpictureflip);
                    break;
                case "Showers":
                    gifTextView.setBackgroundResource(R.mipmap.rainnightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.darkpictureflip);
                    break;
                case "Few Showers":
                    gifTextView.setBackgroundResource(R.mipmap.rainnightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.darkpictureflip);
                    break;
                case "Light Rain":
                    gifTextView.setBackgroundResource(R.mipmap.rainnightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.darkpictureflip);
                    break;
                case "Snow":
                    gifTextView.setBackgroundResource(R.mipmap.snownightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.darkpictureflip);
                    break;
                case "Snow Showers":
                    gifTextView.setBackgroundResource(R.mipmap.snownightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.darkpictureflip);
                    break;
                default:
                    gifTextView.setBackgroundResource(R.mipmap.rainnightbackground);
                    recyclerView.setBackgroundResource(R.mipmap.darkpictureflip);
            }
        }
    }

    public void toDetailedWeather(View view) {
        Intent intent = new Intent(this, DetailedWeather.class);
        Gson gson = new Gson();
        intent.putExtra("ServerModel", gson.toJson(serverModel));
        startActivity(intent);
    }
    public void toCloth(View view) {
        Intent intent = new Intent(this, ClothActivity.class);
        Gson gson = new Gson();
        intent.putExtra("ServerModel", gson.toJson(serverModel));
        startActivity(intent);
    }
    public void toFood(View view) {
        Intent intent = new Intent(this, FoodActivity.class);
        Gson gson = new Gson();
        intent.putExtra("ServerModel", gson.toJson(serverModel));
        startActivity(intent);
    }
    public void toActivity(View view) {
        Intent intent = new Intent(this, ActivityActivity.class);
        Gson gson = new Gson();
        intent.putExtra("ServerModel", gson.toJson(serverModel));
        startActivity(intent);
    }
    public void toTodoListAdd(View view) {
        Intent intent = new Intent(this, Todo_List_Set.class);
        intent.putExtra("Type", "ADD");
        startActivityForResult(intent,3);
    }
}
